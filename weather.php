<?php
/**
 * Created by PhpStorm.
 * User: elu
 * Date: 22/12/2017
 * Time: 6.14
 */

// Called for weather API call.

function getWeather() {
    $mem = new Memcached();
    $mem->addServer("127.0.0.1", 11211);

    $temperature = $mem->get("temperature");
    $weatherCondition = $mem->get("weatherCondition");
    $time = $mem->get("time");

    // Get data from API if not present in memcache.
    if (!$temperature && !$weatherCondition && !$time) {
        $url = file_get_contents("http://api.openweathermap.org/data/2.5/weather?id=648901&appid=2291591cb8ddef647c69c2af0f081b39");
        $json = json_decode($url);

        // Set data from API to memcache after parsing XML.
        $time = $json->dt;
        $mem->set("time", $time, 3600) or die("Couldn't save anything to memcache...");

        $temperature = $json->main->{'temp'};
        $mem->set("temperature", $temperature, 3600) or die("Couldn't save anything to memcache...");

        foreach ($json->weather as $weather) {
            if (isset($weather->{'main'})) {
                $weatherCondition = $weather->{'main'};
                $mem->set("weatherCondition", $weatherCondition, 3600) or die("Couldn't save anything to memcache...");
            }
        }
    }

    date_default_timezone_set('UTC');
    $currentWeather = "Lappeenranta klo: " . $time . "<ul>" . "<li>" . floatval($temperature - 273.15) . "&deg;C</li>" . "<li>" . $weatherCondition . "</li>" . "</ul>";
    return $currentWeather;
}