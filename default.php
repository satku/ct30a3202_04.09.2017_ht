<?php
/**
 * Created by IntelliJ IDEA.
 * User: elu
 * Date: 21/11/2017
 * Time: 19.36
 */

// Loaded if user or guest is logged.
?>
<input name="note" id="noteToAdd" placeholder="Muista..."/>
<input type="submit" class="btn btn-outline-primary" id="addNote" value='Lisää muistioon'/>
<div class="table-hover table-responsive">
    <table class="table" id="notes"></table>
</div>

<script>updateNotes();</script>
<?php