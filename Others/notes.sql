-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 22, 2017 at 11:09 AM
-- Server version: 10.1.26-MariaDB-0+deb9u1
-- PHP Version: 7.0.19-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `www`
--

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int(11) NOT NULL,
  `note` varchar(255) NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `username` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`id`, `note`, `time`, `username`) VALUES
(245, 'kolmivaihekilowattituntimittari', '2017-12-21 21:14:03', 'phpmyadmin'),
(249, 'Miten se nyt menikään.', '2017-12-21 23:29:45', 'phpmyadmina'),
(250, 'Kiiveta', '2017-12-21 23:29:47', 'phpmyadmina'),
(251, 'Hetkonen.', '2017-12-22 02:30:08', 'phpmyadmin'),
(252, 'Henkesi edestä.', '2017-12-22 02:30:12', 'phpmyadmin'),
(253, 'Ja juokse pakoon.', '2017-12-22 02:30:16', 'phpmyadmin'),
(254, 'Eihän tää nyt oikee lähe.', '2017-12-22 02:30:25', 'phpmyadmin'),
(255, 'Henkesi edestä.', '2017-12-22 02:38:20', 'phpmyadmina'),
(256, 'Ja juokse pakoon.', '2017-12-22 02:38:23', 'phpmyadmina'),
(257, 'Miten se nyt menikään.', '2017-12-22 03:43:23', 'phpmyadmi'),
(258, 'Henkesi edestä.', '2017-12-22 03:43:28', 'phpmyadmi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=259;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
