<?php
/**
 * Created by IntelliJ IDEA.
 * User: elu
 * Date: 16/10/2017
 * Time: 14.36
 */

//drop efekti uusille noteille
//iideet classeiksi default.phpssa
//pitkät sanat muistiinpanoissa kusee responsiivisuuden
//formit punaiseksi tai vihreäksi kun syöte väärä/oikea
//uusi tunnus formi järkevän näköiseksi kapealle näytölle
//hallitse sivun taulukko järkeväksi
//servun/tietokannan kello oikeaan aikaan
//footteriin weather.php sisältö, kun memcached asennettu, tai jonnekkin järkevämpään paikkaan
//use strictin käyttö!
//loading icon järkevämpään paikkaan index.phpeestä

// SEO hakukoneoptimointi
// Yksikkötestit
// Kolmannen osapuolen palvelun käyttö (esim. FB-login, [GA ei riitä])

// Called first in index.php.

session_start();

function siteHeader() {
    ?>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <title>Muistikirja</title>
            <!-- Required meta tags -->
            <meta charset="UTF-8">
            <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
            <!-- Bootsrap CSS and My CSS and Font -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
            <link rel="stylesheet" type="text/css" href="styles.css"/>
        </head>
        <body>
            <!-- Jquery -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
            <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
            <!-- My JavaScript -->
            <script src="javascript.js"></script>
    <?php
    include("weather.php");
}

function siteNavigation() {
    ?>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="#">Muistio</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Etusivu<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <?php
                    if (!isset($_SESSION["username"]) || $_SESSION["username"] == "guest") {
                        echo "<a class='nav-link' href='index.php?p=login'>Sisään</a>";
                    } else {
                        echo "<a class='nav-link' href='index.php?p=logout'>Ulos</a>";
                    }
                    ?>
                </li>
                <li class="nav-item">
                    <?php
                    if ($_SESSION['username'] == "phpmyadmin") {
                        echo "<a class='nav-link' href='index.php?p=manage'>Hallitse</a>";
                    } else {
                        echo "<a class='nav-link disabled' href='#'>Hallitse</a>";
                    }
                    ?>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Hae" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Hae</button>
            </form>
        </div>
    </nav>
    <?php
}

function siteFooter() {
    ?>
            <div class="container">
                <div class="main-wrapper">
                    <footer>

                        <div class="row">

                            <div class="col-sm-4">

                                <?php
                                echo getWeather();
                                ?>

                            </div>

                        </div>

                        <div class="copy clearfix">

                            <p class="pull-right"><a href="#">Takaisin ylös</a></p>
                            <p class="pull-left">&copy; 2017 &middot; E.H.</p>

                        </div>

                        <!-- Bootstrap -->
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
                        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

                    </footer>
                </div>
            </div>

        </body>
    </html>
    <?php
}

$err = "<p>Salasanan täytyy olla yli 8 merkkiä pitkä sekä sen 
tulee sisältää sekä isoja että pieniä kirjaimia ja numeroita,
mutta ei muita merkkejä.</p>";

// Check user input on form submit for Html and incorrect input.
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["password2"])) {
        $_POST["username"] = htmlspecialchars($_POST["username"]);
        if (strlen($_POST["password"]) < 9 || strlen($_POST["password"]) > 255) {
            unset($_POST["password"]);
            $_SESSION["err"] = "<p>Salasana on liian lyhyt</p>" . $err;
        } else if (preg_match('/[^A-Za-z0-9]/', $_POST["password"]) || !preg_match('`[A-Z]`', $_POST["password"]) || !preg_match('`[a-z]`', $_POST["password"]) || !preg_match('`[0-9]`', $_POST["password"])) {
            unset($_POST["password"]);
            $_SESSION["err"] = "<p>Tarkista salasana</p>" . $err;
        }
    }
    if (isset($_POST["username"]) && isset($_POST["password"]) && !isset($_POST["password2"])) {
        $_POST["username"] = htmlspecialchars($_POST["username"]);
        $_POST["password"] = htmlspecialchars($_POST["password"]);
    }
}

?>