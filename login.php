<?php
/**
 * Created by IntelliJ IDEA.
 * User: elu
 * Date: 21/11/2017
 * Time: 20.07
 */

// Called on login form.
if(isset($_POST["username"]) && isset($_POST["password"])) {
    try {
        $db = new PDO('mysql:host=localhost;dbname=www;charset=utf8','rwww', 'MgKuNYurIhaflYd9');
        $stmt = $db->prepare("SELECT pwhash FROM users WHERE username=:user");
        $stmt->execute(array(":user" => $_POST["username"]));
    }
    catch (PDOException $e) {
        error_log("Connection to database failed: " . $e->getMessage(), 0);
        echo '<p>Virhe, kokeile kirjautua myöhemmin uudelleen ":D"</p>';
    }

    if (isset($stmt)) {
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // Database request returned only 1 user.
        if (count($rows) === 1) {
            if (password_verify($_POST["password"], $rows[0]["pwhash"])) {
                // Change possible guest notes to be owned by logged in user.
                if ($_SESSION['username'] = "guest") {
                    try {
                        $db = new PDO('mysql:host=localhost;dbname=www;charset=utf8', 'www', 'Y7EsahJnkKeXTeRH');
                        $stmt = $db->prepare("UPDATE notes SET username=:name WHERE notes.username = 'guest'");
                        $stmt->execute(array(":name" => $_POST["username"]));
                    } catch (PDOException $e) {
                        error_log("Connection to database failed: " . $e->getMessage(), 0);
                        echo '<p>Virhe, kokeile kirjautua myöhemmin uudelleen ":D"</p>';
                    }
                }
                $_SESSION['username'] = $_POST["username"];
                echo "<p>Kirjautuminen onnistui</p>";
                header("refresh:1;url=index.php");
            } else {
                echo "<p>Tarkista salasana ja käyttäjätunnus</p>";
                header("refresh:1;url=index.php");
            }
        } else {
            error_log("Two accounts with the same username exists in database!", 0);
            echo '<p>Virhe, kokeile kirjautua myöhemmin uudelleen ":D"</p>';
        }
    }

} else {
    ?>
    <form action="index.php?p=login" method="post">
        <input type="text" name="username" placeholder="Käyttäjänimi"/>
        <input type="password" name="password" placeholder="Salasana"/>
        <input type="submit" value="Kirjaudu"/>
        <p><br>Eikö sinulla ole tunnusta?<br><a href='index.php?p=register'>Luo uusi tunnus</a></p>
    </form>
    <span class="error"><?php echo $_SESSION["err"];unset($_SESSION["err"]);?></span>
    <?php
}