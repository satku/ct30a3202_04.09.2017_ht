<?php
/**
 * Created by IntelliJ IDEA.
 * User: elu
 * Date: 16/10/2017
 * Time: 16.29
 */

session_start();

// Called on note add.
if(isset($_POST["note"]) && $_POST["note"] != "" && isset($_SESSION["username"]) && !isset($_POST["id"])) {
    try {
        $db = new PDO('mysql:host=localhost;dbname=www;charset=utf8', 'www', 'Y7EsahJnkKeXTeRH');
        $stmt = $db->prepare("INSERT INTO notes(note, username) VALUES(:text, :user)");
        $stmt->execute(array(":text" => $_POST["note"], ":user" => $_SESSION["username"]));
    }
    catch (PDOException $e) {
        error_log("Connection to database failed: " . $e->getMessage(), 0);
        echo '<p>Virhe, kokeile toimintoa myöhemmin uudelleen ":D"</p>';
    }
}

// Called on note delete.
if(isset($_POST["id"]) && !isset($_POST["note"]) && isset($_SESSION["username"])) {
    try {
        $db = new PDO('mysql:host=localhost;dbname=www;charset=utf8', 'www', 'Y7EsahJnkKeXTeRH');
        $stmt = $db->prepare("DELETE FROM notes WHERE id=:number");//vaiha tämä executeen
        $stmt->execute(array(":number" => $_POST["id"]));
    }
    catch (PDOException $e) {
        error_log("Connection to database failed: " . $e->getMessage(), 0);
        echo '<p>Virhe, kokeile toimintoa myöhemmin uudelleen ":D"</p>';
    }
}

// Called on page load or after note add to update userview.
if(!isset($_POST["id"]) && !isset($_POST["note"]) && isset($_SESSION["username"])) {
    try {
        $db = new PDO('mysql:host=localhost;dbname=www;charset=utf8', 'rwww', 'MgKuNYurIhaflYd9');
        $stmt = $db->prepare("SELECT id, note FROM notes WHERE username=:user");
        $stmt->execute(array(":user" => $_SESSION["username"]));
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        print(json_encode($rows));
    }
    catch (PDOException $e) {
        error_log("Connection to database failed: " . $e->getMessage(), 0);
        echo '<p>Virhe, kokeile toimintoa myöhemmin uudelleen ":D"</p>';
    }
}

?>