<?php

require_once("utils.php");

siteHeader();
siteNavigation();

?>
<main role="main" class="container">
    <div class="starter-template">
        <h1>Muistikirja muistihäröisille</h1>
<?php

if (!isset($_SESSION['username']) && !isset($_GET["p"])) {
    echo "<p class='lead'>Tervetuloa huolettomat päivät.<br>Meidän avulla tärkeimmätkin muistiinpanosi pysyvät tallessa.</p>";
    require("login.php");
    echo "<a class='guestLogin' href='index.php?p=guest'>Tai jatka tästä vieraana</a>";
} else if ($_GET["p"] === "login") {
    require("login.php");
} else if ($_GET["p"] === "register") {
    require("register.php");
} else if ($_GET["p"] === "logout") {
    require("logout.php");
} else if ($_GET["p"] === "guest") {
    $_SESSION['username'] = "guest";
    require("default.php");
    echo "<p><br><a href='index.php?p=register'>Luo uusi tunnus</a></p>";
} else if ($_GET["p"] === "manage") {
    require("manage.php");
} else {
    require("default.php");
}
include("runner-309595.svg");
?>
    </div>
</main><!-- /.container -->
<?php

siteFooter();