<?php
/**
 * Created by IntelliJ IDEA.
 * User: elu
 * Date: 21/11/2017
 * Time: 20.07
 */

// Called on register form.
if (isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["password2"])) {
    if ($_POST["password"] == $_POST["password2"]) {
        $pwhash = password_hash($_POST["password"], PASSWORD_DEFAULT, ["cost" => 13]);

        try {
            $db = new PDO('mysql:host=localhost;dbname=www;charset=utf8', 'www', 'bmOP6XdNbVFjKm3T');
            $stmt = $db->prepare("INSERT INTO users(username, pwhash) SELECT * FROM (SELECT :name, :pass) AS tmp WHERE NOT EXISTS (SELECT username FROM users WHERE username=:name) LIMIT 1");
            $stmt->execute(array(":name" => $_POST["username"], ":pass" => $pwhash));

            // Database request didn't return a user.
            if ($stmt->rowCount()) {
                // Change possible guest notes to be owned by logged in user.
                if ($_SESSION['username'] = "guest") {
                    try {
                        $db = new PDO('mysql:host=localhost;dbname=www;charset=utf8', 'www', 'bmOP6XdNbVFjKm3T');
                        $stmt = $db->prepare("UPDATE notes SET username=:name WHERE notes.username = 'guest'");
                        $stmt->execute(array(":name" => $_POST["username"]));
                    } catch (PDOException $e) {
                        error_log("Connection to database failed: " . $e->getMessage(), 0);
                        echo '<p>Virhe, kokeile rekisteröityä myöhemmin uudelleen ":D"</p>';
                    }
                }
                echo "<p>Käyttäjätili luotu</p>";
                $_SESSION['username'] = $_POST["username"];
                header("refresh:1;url=index.php");
            } else {
                echo "<p>Syöttämäsi käyttäjätunnus on jo käytössä</p>";
                header("refresh:1;url=index.php?p=register");
            }

        } catch (PDOException $e) {
            unset($_SESSION['username']);
            error_log("Connection to database failed: " . $e->getMessage(), 0);
            echo '<p>Virhe, kokeile rekisteröityä myöhemmin uudelleen ":D"</p>';
        }

    } else {
        echo "<p>Salasanat eivät täsmää</p>";
        header("refresh:1;url=index.php?p=register");
    }

} else {
    ?>
    <form action='index.php?p=register' method="post">
            <input type="text" name="username" placeholder="Käyttäjänimi"/>
            <input type="password" name="password" placeholder="Salasana"/>
            <input type="password" name="password2" placeholder="Salasana uudestaan"/>
            <input type="submit" value="Rekisteröidy"/>
    </form>
    <span class="error"><?php echo $_SESSION["err"];unset($_SESSION["err"]);?></span>
    <?php
}