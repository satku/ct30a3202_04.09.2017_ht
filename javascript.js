"use strict";

// Update/get notes from database to userview.
function updateNotes() {
    $.getJSON("handler.php", function(data) {
        var text = "";
        data.forEach(function(entry) {
            text += "<tr><td>" + entry.note + "</td>" + "<td>" + "<input type='button' class='btn btn-outline-danger' id='" + entry.id + "' value='Poista'/>" + "</td></tr>";
        });
        document.getElementById("notes").innerHTML = text;
    });
}

// Strip possible Html code from user added note.
function escapeHtml(text) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };

    return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

$(document).ready(function() {
    // Show loading icon on Ajax calls.
    var $loading = $('.svg').hide();
    $(document)
        .ajaxStart(function () {
            $loading.show();
        })
        .ajaxStop(function () {
            $loading.hide();
        });

    // Add note to database and userview.
    $('#addNote').click(function() {
        var note = escapeHtml($("#noteToAdd").val());
        $("#noteToAdd").val('');
        var addButton = $("addNote");

        addButton.attr("disabled", "disabled");
        addButton.text("Lisätään...");
        var request = $.ajax({
            url: "handler.php",
            type: "POST",
            data: {"note": note},
            dataType: "html"
        });
        request.done(function() {
            addButton.removeAttr("disabled");
            addButton.text("Lisää muistioon");
            updateNotes();
        });
        request.fail(function(jqXHR, textStatus) {
            alert(textStatus);
            addButton.removeAttr("disabled");
            addButton.text("Listää muistioon");
        });

    });

    // Delete note from database and userview.
    $(document).on("click", ":button", function() {
        var thisId = this.id;
        var thisNote = $(this);

        function remove() {
            thisNote.closest("tr").fadeOut(400, function() {
                thisNote.closest("tr").remove();
            });
        }

        var request = $.ajax({
            url: "handler.php",
            type: "POST",
            data: {"id": thisId},
            dataType: "html"
        });
        request.done(function() {
            remove();
        });
        request.fail(function(jqXHR, textStatus) {
            alert(textStatus);
        });

    });

});