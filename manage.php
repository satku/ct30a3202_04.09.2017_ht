<?php
/**
 * Created by IntelliJ IDEA.
 * User: elu
 * Date: 21/12/2017
 * Time: 21.23
 */

// Only for Admin user.
if ($_SESSION["username"] == "phpmyadmin") {
    // Get all user ids from database.
    try {
        $db = new PDO('mysql:host=localhost;dbname=www;charset=utf8', 'rwww', 'MgKuNYurIhaflYd9');
        $stmt = $db->prepare("SELECT uid FROM users ORDER BY username");
        $stmt->execute();
        $userId = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e) {
        error_log("Connection to database failed: " . $e->getMessage(), 0);
        echo '<p>Virhe, kokeile toimintoa myöhemmin uudelleen ":D"</p>';
    }

    // Get all usernames and counts of notes from database.
    try {
        $db = new PDO('mysql:host=localhost;dbname=www;charset=utf8', 'rwww', 'MgKuNYurIhaflYd9');
        $stmt = $db->prepare("SELECT username, COUNT(*) FROM notes GROUP BY username");
        $stmt->execute();
        $userRows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e) {
        error_log("Connection to database failed: " . $e->getMessage(), 0);
        echo '<p>Virhe, kokeile toimintoa myöhemmin uudelleen ":D"</p>';
    }

    // Get last time note was added, from database.
    try {
        $db = new PDO('mysql:host=localhost;dbname=www;charset=utf8', 'rwww', 'MgKuNYurIhaflYd9');
        $stmt = $db->prepare("SELECT time FROM notes s1 WHERE time = (SELECT MAX(time) FROM notes s2 WHERE s1.username = s2.username) ORDER BY username");
        $stmt->execute();
        $timeRows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e) {
        error_log("Connection to database failed: " . $e->getMessage(), 0);
        echo '<p>Virhe, kokeile toimintoa myöhemmin uudelleen ":D"</p>';
    }

    // Push all the user info to three tables.
    if (isset($userId) && isset($userRows) && isset($timeRows)) {
        $table = array("Id" => array(),"User" => array(),"Notes" => array(),"Time" => array());

        echo "<div class='container'>";
        echo "<div class='row'>";

        $data = "<table><tr><td>Id</td></tr>";
        foreach ($userId as $idIndex) {
            $data = $data . "<tr>";
            foreach ($idIndex as $idIndex2) {
                $table["Id"][] = $idIndex2;
                $data = $data . "<td>$idIndex2\n</td>";
            }
            $data = $data . "</tr>";
        }
        unset($idIndex);
        unset($idIndex2);
        $data = $data . "</table>";
        echo $data;

        $counter = 0;
        $data = "<table><tr><td>User</td><td>Notes</td></tr>";
        foreach ($userRows as $userIndex) {
            $data = $data . "<tr>";
            foreach ($userIndex as $userIndex2) {
                // Every other is username and other is a notecount.
                if ($counter % 2 == 0) {
                    $table["User"][] = $userIndex2;
                } else {
                    $table["Notes"][] = $userIndex2;
                }
                $counter++;
                $data = $data . "<td>$userIndex2\n</td>";
            }
            $data = $data . "</tr>";
        }
        unset($userIndex);
        unset($userIndex2);
        $data = $data . "</table>";
        echo $data;

        $data = "<table><tr><td>Time</td></tr>";
        foreach ($timeRows as $timeIndex) {
            $data = $data . "<tr>";
            foreach ($timeIndex as $timeIndex2) {
                $table["Time"][] = $timeIndex2;
                $data = $data . "<td>$timeIndex2\n</td>";
            }
            $data = $data . "</tr>";
        }
        unset($timeIndex);
        unset($timeIndex2);
        $data = $data . "</table>";
        echo $data;

        echo "</div>";
        echo "</div>";
    }
}