<?php
/**
 * Created by IntelliJ IDEA.
 * User: elu
 * Date: 21/11/2017
 * Time: 22.31
 */

// Called on user logout.

session_destroy();
echo "<p>Uloskirjautuminen onnistui</p>";
header("refresh:1;url=index.php");